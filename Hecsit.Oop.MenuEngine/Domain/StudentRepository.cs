﻿using System.Collections.Generic;

namespace Hecsit.Oop.MenuEngine.Domain
{
	public class StudentRepository
	{
		private readonly List<Student> _students = new List<Student>();

		public ICollection<Student> GetAll()
		{
			return _students.AsReadOnly();
		} 

		public void Add(Student student)
		{
			_students.Add(student);
		}
	}
}