﻿using System;

namespace Hecsit.Oop.MenuEngine.Domain
{
	public class Student
	{
		public string FullName { get; set; } 
		public int Year { get; set; } 
		public DateTime EnrollmentDate { get; set; }

		public Student(string fullName, int year, DateTime enrollmentDate)
		{
			FullName = fullName;
			Year = year;
			EnrollmentDate = enrollmentDate;
		}
	}
}