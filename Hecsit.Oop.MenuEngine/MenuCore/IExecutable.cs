﻿namespace Hecsit.Oop.MenuEngine.MenuCore
{
	public interface IExecutable
	{
		void Execute();
	}
}