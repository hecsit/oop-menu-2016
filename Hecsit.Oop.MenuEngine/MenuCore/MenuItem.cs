﻿namespace Hecsit.Oop.MenuEngine.MenuCore
{
	public class MenuItem
	{
		private readonly string _title;
		private readonly IExecutable _executable;

		public string Title
		{
			get { return _title; }
		}

		public IExecutable Executable
		{
			get { return _executable; }
		}

		public MenuItem(string title, IExecutable executable)
		{
			_title = title;
			_executable = executable;
		}
	}
}