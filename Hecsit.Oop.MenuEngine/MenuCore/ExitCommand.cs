﻿using System;

namespace Hecsit.Oop.MenuEngine.MenuCore
{
	public class ExitCommand : IExecutable
	{
		public void Execute()
		{
			Environment.Exit(0);
		}
	}
}