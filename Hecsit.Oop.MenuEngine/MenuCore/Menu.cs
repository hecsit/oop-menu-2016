﻿using System;
using System.Collections.Generic;

namespace Hecsit.Oop.MenuEngine.MenuCore
{
	public class Menu
	{
		private readonly string _title;
		private readonly List<MenuItem> _items = new List<MenuItem>();

		public Menu(string title)
		{
			_title = title;
		}


		public Menu WithItem(string itemTitle, IExecutable executable)
		{
			_items.Add(new MenuItem(itemTitle, executable));
			return this;
		}

		public Menu WithItem(string itemTitle, Action action)
		{
			_items.Add(new MenuItem(itemTitle, new DelegateCommandAdapter(action)));
			return this;
		}

		public Menu WithExit(string itemTitle)
		{
			_items.Add(new MenuItem(itemTitle, new ExitCommand()));
			return this;
		}

		public void Run()
		{
			while (true)
			{
				WriteMenu();
				var item = ReadItem();
				ExecuteItem(item);
			}
		}

		private void ExecuteItem(int itemNumber)
		{
			var item = _items[itemNumber - 1];
			item.Executable.Execute();
		}

		private int ReadItem()
		{
			Console.Write("Please, choose item: ");
			var itemText = Console.ReadLine();

			return int.Parse(itemText);
		}

		private void WriteMenu()
		{
			Console.WriteLine(_title);
			for (var number = 1; number <= _items.Count; ++number)
			{
				Console.WriteLine($"{number} - {_items[number-1].Title}");
			}
		}
	}
}