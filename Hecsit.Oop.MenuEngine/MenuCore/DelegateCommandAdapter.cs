﻿using System;

namespace Hecsit.Oop.MenuEngine.MenuCore
{
	public class DelegateCommandAdapter : IExecutable
	{
		private readonly Action _action;

		public DelegateCommandAdapter(Action action)
		{
			_action = action;
		}

		public void Execute()
		{
			_action.Invoke();
		}
	}
}