﻿
using System;
using Hecsit.Oop.MenuEngine.Domain;
using Hecsit.Oop.MenuEngine.MenuCore;
using Hecsit.Oop.MenuEngine.UI;

namespace Hecsit.Oop.MenuEngine
{
	class Program
	{
		static void Main(string[] args)
		{
			var repository = new StudentRepository();

			new Menu("Select Action please")
				.WithItem("Add New Student", 
					new ProfileAspect(
						new HighlightAspect(
							new AddStudentCommand(repository)
						),
						"Add Student"
					)
				)
				.WithItem("Show All Students", new ShowAllStudentsCommand(repository))
				.WithItem("Get Time", () => Console.WriteLine(DateTime.Now))
				.WithExit("Go Home")
				.Run();
		}
	}
}
