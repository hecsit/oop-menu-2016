﻿using System;
using Hecsit.Oop.MenuEngine.Domain;
using Hecsit.Oop.MenuEngine.MenuCore;

namespace Hecsit.Oop.MenuEngine.UI
{
	public class AddStudentCommand : IExecutable
	{
		private readonly StudentRepository _repository;

		public AddStudentCommand(StudentRepository repository)
		{
			_repository = repository;
		}

		public void Execute()
		{
			Console.Write("Enter Full Name: ");
			var fullName = Console.ReadLine();

			Console.Write("Enter Year: ");
			var year = int.Parse(Console.ReadLine());

			Console.Write("Enter Enrollment Date: ");
			var enrollmentDate = DateTime.Parse(Console.ReadLine());

			var student = new Student(fullName, year, enrollmentDate);
			_repository.Add(student);

			Console.WriteLine("Student has been added.");
		}
	}
}