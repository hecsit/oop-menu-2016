﻿using System;
using Hecsit.Oop.MenuEngine.MenuCore;

namespace Hecsit.Oop.MenuEngine.UI
{
	public class HighlightAspect : IExecutable
	{
		private readonly IExecutable _wrapped;

		public HighlightAspect(IExecutable wrapped)
		{
			_wrapped = wrapped;
		}

		public void Execute()
		{
			var oldColor = Console.ForegroundColor;
			Console.ForegroundColor = ConsoleColor.Red;

			_wrapped.Execute();

			Console.ForegroundColor = oldColor;
		}
	}
}