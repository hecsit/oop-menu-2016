﻿using System;
using Hecsit.Oop.MenuEngine.Domain;
using Hecsit.Oop.MenuEngine.MenuCore;

namespace Hecsit.Oop.MenuEngine.UI
{
	public class ShowAllStudentsCommand : IExecutable
	{
		private readonly StudentRepository _repository;

		public ShowAllStudentsCommand(StudentRepository repository)
		{
			_repository = repository;
		}

		public void Execute()
		{
			var students = _repository.GetAll();
			if (students.Count == 0)
			{
				Console.WriteLine("No students were found.");
			}

			foreach (var student in students)
			{
				Console.WriteLine($"{student.FullName}, {student.Year} year ({student.EnrollmentDate}");
			}
		}
	}
}