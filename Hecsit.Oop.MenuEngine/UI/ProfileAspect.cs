﻿using System.Diagnostics;
using Hecsit.Oop.MenuEngine.MenuCore;

namespace Hecsit.Oop.MenuEngine.UI
{
	public class ProfileAspect : IExecutable
	{
		private readonly IExecutable _wrapped;
		private readonly string _title;

		public ProfileAspect(IExecutable wrapped, string title)
		{
			_wrapped = wrapped;
			_title = title;
		}

		public void Execute()
		{
			var stopwatch = Stopwatch.StartNew();

			_wrapped.Execute();

			stopwatch.Stop();
			Debug.WriteLine($"Execution time of {_title}: {stopwatch.Elapsed}");
		}
	}
}